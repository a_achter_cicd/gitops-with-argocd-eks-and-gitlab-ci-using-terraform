# Adding Backend as S3 for Remote State Storage
    bucket         = "ops100-eks-argocd-2023-02"
    key            = "terraform-argo.tfstate"
    region         = "eu-west-1" # Change this to your desired AWS region
    dynamodb_table = "ops100-argo-state-lock"
    encrypt        = true
