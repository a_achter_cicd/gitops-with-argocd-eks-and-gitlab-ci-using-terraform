/*
data "terraform_remote_state" "eks" {
  backend = "local"

  config = {
    path = "${path.module}/../eks/terraform.tfstate"
  }
}
*/

// get the remote state data for eks
data "terraform_remote_state" "eks" {
  backend = "s3"

  config = {
    bucket         = "ops100-eks-argocd-2023-02"
    key            = "terraform.tfstate"
    region         = "eu-west-1" # Change this to your desired AWS region
    dynamodb_table = "ops100-tf-state-lock"
  }
}
