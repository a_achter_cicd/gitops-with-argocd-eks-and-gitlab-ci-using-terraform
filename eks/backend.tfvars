# Configure the Terraform backend to use the created S3 bucket and DynamoDB table for state storage and locking
    bucket         = "ops100-eks-argocd-2023-02"
    key            = "terraform.tfstate"
    region         = "eu-west-1" # Change this to your desired AWS region
    dynamodb_table = "ops100-tf-state-lock"
    encrypt        = true
