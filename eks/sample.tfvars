cluster_name       = "ops078a-argo"
name_prefix        = "dev"
main_network_block = "10.0.0.0/16"

eks_managed_node_groups = {
  "devops-eks-ondemand" = {
    ami_type     = "AL2_x86_64"
    min_size     = 2
    max_size     = 4
    desired_size = 2
    instance_types = [
      "t2.medium",
    ]
    capacity_type = "ON_DEMAND"
    network_interfaces = [{
      delete_on_termination       = true
      associate_public_ip_address = true
    }]
  }
}

external_dns_iam_role      = "external-dns"
external_dns_chart_name    = "external-dns"
external_dns_chart_repo    = "https://kubernetes-sigs.github.io/external-dns/"
external_dns_chart_version = "1.9.0"

external_dns_values = {
  "image.repository"   = "k8s.gcr.io/external-dns/external-dns",
  "image.tag"          = "v0.11.0",
  "logLevel"           = "info",
  "logFormat"          = "json",
  "triggerLoopOnEvent" = "true",
  "interval"           = "5m",
  "policy"             = "sync",
  "sources"            = "{ingress}"
}

dns_hosted_zone              = "ops100.info"
load_balancer_name           = "aws-load-balancer-controller"
alb_controller_iam_role      = "load-balancer-controller"
alb_controller_chart_name    = "aws-load-balancer-controller"
alb_controller_chart_repo    = "https://aws.github.io/eks-charts"
alb_controller_chart_version = "1.4.7"

admin_users     = ["admin-ops078a"]
developer_users = ["dev-ops078a", "test-ops078a"]
root_users = ["root"]
