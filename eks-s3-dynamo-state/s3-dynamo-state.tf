provider "aws" {
  region = "eu-west-1" # Change this to your desired AWS region
}

# Create an S3 bucket to store the Terraform state files
resource "aws_s3_bucket" "tf_ops100_eks_argocd" {
  bucket = "ops100-eks-argocd-2023-02" # Change this to a unique bucket name
  acl    = "private"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

# Create an Amazon DynamoDB table for Terraform state locking
resource "aws_dynamodb_table" "tf_state_lock_table" {
  name           = "ops100-tf-state-lock" # Change this to a unique table name
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Terraform   = "true"
    Environment = "ops100" # Customize as per your environment
  }
}
/*
# Configure the Terraform backend to use the created S3 bucket and DynamoDB table for state storage and locking
terraform {
  backend "s3" {
    bucket         = "ops100-eks-argocd-2023-02"
    key            = "terraform.tfstate"
    region         = "eu-west-1" # Change this to your desired AWS region
    dynamodb_table = "ops100-tf-state-lock"
    encrypt        = true
  }
}

# ORG Configure the Terraform backend to use the created S3 bucket and DynamoDB table for state storage and locking
terraform {
  backend "s3" {
    bucket         = aws_s3_bucket.tf_state_bucket.bucket
    key            = "terraform.tfstate"
    region         = "eu-west-1" # Change this to your desired AWS region
    dynamodb_table = aws_dynamodb_table.tf_state_lock_table.name
    encrypt        = true
  }
}







*/
